section .text

%include "lib.inc"

global find_word


; rdi - указатель на нуль-терминированную строку
; rsi - указатель на начало словаря
; Проходит по словарю в поисках подходящего ключа
; Возвращает адрес начала вхождения в словарь, иначе - 0
find_word:
	xor rax, rax
	.next:
		cmp rsi, 0
		jz .end
		push rsi
		add rsi, 8
		call string_equals
		cmp rax, 0
		jnz .success
		pop rsi
		mov rsi, [rsi]
		jmp .next
	.success:
		mov rax, rsi
		ret
	.end:
		xor rax, rax
		ret
