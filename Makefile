ASM=nasm -f elf64 -o

all: lib.o dict.o main.o
	ld -o $@ $^

lib.o: lib.asm
	$(ASM) $@ $<

dict.o: dict.asm lib.o lib.inc
	$(ASM) $@ $<

main.o: main.asm dict.o words.inc colon.inc
	$(ASM) $@ $<

clean:
	rm -rf *.o all
