%include "colon.inc"

section .bss
BUFFER_SIZE: resb 256
MARK_SIZE: resb 8

section .rodata
%include "words.inc"
not_found: db "Key's not found", 0
out_of_size: db "Key doesn't fit buffer size", 0

section .text
%include "lib.inc"
extern find_word

global _start

; Читает строку размером не более 255 в буфер с stdin
; Пытается найти вхождение в словаре
; Если оно найдено, распечатывает в stdout значение по этому ключу
; Иначе выдает сообщение об ошибке в stderr
_start:
	mov rsi, BUFFER_SIZE
	sub rsp, BUFFER_SIZE
	mov rdi, rsp
	call read_word
	cmp rax, 0
	jz .out_of_size
	mov rdi, rax
	mov rsi, next
	call find_word
	cmp rax, 0
	jz .not_found
	add rax, MARK_SIZE
	mov rdi, rax
	call string_length
	add rdi, rax
	inc rdi
	call print_string
	call print_newline
	call exit
	.not_found:
		mov rdi, not_found
		call print_error
		call print_newline
		call exit
	.out_of_size:
		mov rdi, out_of_size
		call print_error
		call print_newline
		call exit


